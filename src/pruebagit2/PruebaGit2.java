package pruebagit2;

import java.util.Scanner;

/**
 *
 * @author icruzgarcia
 * @since Version 1.7.3 14/01/2014
 */
public class PruebaGit2 {


    /**
     * Esta clase será una utilidad para cambiar la temperatura dada en ºC a
     * ºFarenheit y ºKelvin
     */
    public static void main(String[] args) {
        float celsius, kelvin, farenheit;
        /* celsius,kelvin,farenheit son unidades de medición de temperatura. Aunque el celsius es el 
         * universal, kelvin y farenheit se utilizan en algunos paises
         */
        do{
        System.out.println("Introduza os ºC que desexa cambiar,pulse 0ºC para salir: ");
        Scanner obx = new Scanner(System.in);
        celsius = obx.nextFloat();
        kelvin = (celsius + 273);
        farenheit = (float)(celsius * (9.0 / 5) + 32);
        System.out.println("A equivalencia é de: " + kelvin + " ºK \né de: " + farenheit + " ºF");
        }while(celsius==0);
    }
}
